CC=gcc

CFLAGS=-Wall -Wextra -Werror -O0 -g -std=c11 -I.. 

.PHONY: all clean


all: clean grade

arvore.o: arvore.c
fila.o: fila.c

le.o: le.c

test: arvore.o le.o fila.o test.c
	$(CC) $(CFLAGS) $^ -o test -lm

grade: test
	./test



clean:
	rm -rf *.o test test.dSYM

